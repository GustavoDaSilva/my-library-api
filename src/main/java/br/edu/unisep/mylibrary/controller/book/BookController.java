package br.edu.unisep.mylibrary.controller.book;

import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import br.edu.unisep.mylibrary.domain.dto.book.RegisterBookDto;
import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import br.edu.unisep.mylibrary.domain.usecase.book.*;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/book")
public class BookController {

    private final FindAllBooksUseCase findAllBooks;
    private final FindBookByIdUseCase findById;
    private final RegisterBookUseCase registerBook;
    private final UpdateBookUseCase updateBook;
    private final FindBookByIsbnUseCase findBookByIsbn;
    private final FindBookByTitleUseCase findBookByTitle;

    @GetMapping
    public ResponseEntity<List<BookDto>> findAll() {
        var books = findAllBooks.execute();

        return books.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(books);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> findById(@PathVariable("id") Integer id) {
        var book = findById.execute(id);

        return book == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(book);
    }

    @PostMapping
    public ResponseEntity<Boolean> save(@RequestBody RegisterBookDto book) {
        registerBook.execute(book);
        return ResponseEntity.ok(true);
    }

    @PutMapping
    public ResponseEntity<Boolean> update(@RequestBody UpdateBookDto book) {
        updateBook.execute(book);
        return ResponseEntity.ok(true);
    }

    @GetMapping("/byIsbn/{isbn}")
    public ResponseEntity<BookDto> findByIsbn(@PathVariable("isbn") String isbn) {
        var book = findBookByIsbn.execute(isbn);

        return book == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(book);
    }

    @GetMapping("/byTitle")
    public ResponseEntity<List<BookDto>> findByTitle(@RequestParam("title") String title) {
        var books = findBookByTitle.execute(title);

        return books.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(books);
    }


}
