package br.edu.unisep.mylibrary.domain.usecase.book;

import br.edu.unisep.mylibrary.data.repository.BookRepository;
import br.edu.unisep.mylibrary.domain.builder.book.BookBuilder;
import br.edu.unisep.mylibrary.domain.dto.book.BookDto;
import br.edu.unisep.mylibrary.domain.validator.book.FindBookByIdValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class FindBookByIdUseCase {

    private final FindBookByIdValidator validator;
    private final BookRepository bookRepository;
    private final BookBuilder builder;

    public BookDto execute(Integer bookId) {
        validator.validate(bookId);

        var book = bookRepository.findById(bookId);
        return book.map(builder::from).orElse(null);
    }
}
