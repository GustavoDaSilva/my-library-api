package br.edu.unisep.mylibrary.domain.dto.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterBookDto {

    private String title;

    private String author;

    private Integer edition;

    private Integer publicationYear;

    private String summary;

    private String publisher;

    private Integer pages;

    private String isbn;

}
