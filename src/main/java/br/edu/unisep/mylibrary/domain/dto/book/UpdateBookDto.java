package br.edu.unisep.mylibrary.domain.dto.book;

import lombok.Data;

@Data
public class UpdateBookDto extends RegisterBookDto{

    private Integer id;

}
