package br.edu.unisep.mylibrary.domain.validator.book;

import br.edu.unisep.mylibrary.domain.dto.book.UpdateBookDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.mylibrary.domain.validator.ValidationMessages.MESSAGE_REQUIRED_BOOK_ID;

@Component
@AllArgsConstructor
public class UpdateBookValidator {

    private final RegisterBookValidator registerValidator;

    public void validate(UpdateBookDto book) {
        Validate.notNull(book.getId(), MESSAGE_REQUIRED_BOOK_ID);
        Validate.isTrue(book.getId() > 0, MESSAGE_REQUIRED_BOOK_ID);

        registerValidator.validate(book);
    }

}
